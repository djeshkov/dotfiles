# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Add coreutils bin dir to path
if [ -d /usr/local/opt/coreutils/libexec ]
then
  export PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
  export MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"
fi

# dircolors
if [ -e ~/.dircolors ]
then
  eval `dircolors -b ~/.dircolors`
fi

# Lock and Load a custom theme file
# location /.bash_it/themes/
export BASH_IT_THEME='slick'

# Set this to false to turn off version control status checking within the prompt for all themes
export SCM_CHECK=true
# Set default editor (VIM)

export EDITOR=vim

#alias for vim
alias vi=vim

# git settings

git config --global user.email "d.jeshkov@gmail.com"
git config --global user.name "Daniil Jeshkov"


# Tab complete sudo commands
complete -cf sudo

# Load Bash It
export BASH_IT="$HOME/.bash_it"

if [ -e $BASH_IT/bash_it.sh ]
then
  source $BASH_IT/bash_it.sh
fi

test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"

# Fix vim colors inside tmux
if [ -n $TMUX ]; then
   alias vim="TERM=screen-256color vim"
fi

# Put bash in vim mode
set -o vi

# Path to the bash it configuration
export BASH_IT="/home/pavel/dotfiles/bash-it"

# Lock and Load a custom theme file
# location /.bash_it/themes/
export BASH_IT_THEME='bobby'

# (Advanced): Change this to the name of your remote repo if you
# cloned bash-it with a remote other than origin such as `bash-it`.
# export BASH_IT_REMOTE='bash-it'

# Your place for hosting Git repos. I use this for private repos.
export GIT_HOSTING='git@git.domain.com'

# Don't check mail when opening terminal.
unset MAILCHECK

# Change this to your console based IRC client of choice.
export IRC_CLIENT='irssi'

# Set this to the command you use for todo.txt-cli
export TODO="t"

# Set this to false to turn off version control status checking within the prompt for all themes
export SCM_CHECK=true

# Set Xterm/screen/Tmux title with only a short hostname.
# Uncomment this (or set SHORT_HOSTNAME to something else),
# Will otherwise fall back on $HOSTNAME.
#export SHORT_HOSTNAME=$(hostname -s)

# Set Xterm/screen/Tmux title with only a short username.
# Uncomment this (or set SHORT_USER to something else),
# Will otherwise fall back on $USER.
#export SHORT_USER=${USER:0:8}

# Set Xterm/screen/Tmux title with shortened command and directory.
# Uncomment this to set.
#export SHORT_TERM_LINE=true

# Set vcprompt executable path for scm advance info in prompt (demula theme)
# https://github.com/djl/vcprompt
#export VCPROMPT_EXECUTABLE=~/.vcprompt/bin/vcprompt

# (Advanced): Uncomment this to make Bash-it reload itself automatically
# after enabling or disabling aliases, plugins, and completions.
# export BASH_IT_AUTOMATIC_RELOAD_AFTER_CONFIG_CHANGE=1

# Load Bash It
source "$BASH_IT"/bash_it.sh
